package com.devcamp.j28.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j28.model.CVoucher;
import com.devcamp.j28.repository.IVoucherRepository;

@RestController
@CrossOrigin
public class CVoucherController {
    @Autowired
    IVoucherRepository pIVoucherRepository;

    @PutMapping("/voucher/{ma_voucher}/{phan_tram_giam_gia}")
    public ResponseEntity<Object> capNhatPhanTram(@PathVariable("ma_voucher") String maVoucher,
                                                @PathVariable("phan_tram_giam_gia") String phanTram) {
        CVoucher voucherData = pIVoucherRepository.findByMaVoucher(maVoucher);
        if (voucherData != null) {
            try {
                int discount = pIVoucherRepository.updatePT(maVoucher, phanTram);
                return new ResponseEntity<>(discount, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/voucher")
    public List<CVoucher> getAllVoucher() {
        return pIVoucherRepository.findAll();
    }
}
