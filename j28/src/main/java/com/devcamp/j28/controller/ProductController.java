package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j28.entity.Product;
import com.devcamp.j28.repository.IProductRepository;

@RestController
@CrossOrigin
public class ProductController {
    @Autowired
    IProductRepository productRepository;

    @GetMapping("/products")
    public List<Product> getAllProduct() {
        return productRepository.findAll();
    }

    @GetMapping("/products/{id}")
    public Product getProductById(@PathVariable int id) {
        Optional<Product> productData = productRepository.findById(id);
        if (productData.isPresent()) {
            return productData.get();
        } else {
            return null;
        }
    }

    @PostMapping("/products")
    public ResponseEntity<Object> createNewProduct(@RequestBody Product products) {
        try {
            Product product = new Product();
            product.setIdProductLine(products.getIdProductLine());
            product.setOrderDetails(products.getOrderDetails());
            product.setProductCode(products.getProductCode());
            product.setProductDescripttion(products.getProductDescripttion());
            product.setProductName(products.getProductName());
            product.setProductScale(products.getProductScale());
            product.setProductVendor(products.getProductVendor());
            product.setQuantityInStock(products.getQuantityInStock());
            Product newProduct = productRepository.save(product);
            return new ResponseEntity<>(newProduct, HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id, @RequestBody Product products) {
        Optional<Product> productData = productRepository.findById(id);
        if (productData.isPresent()) {
            Product product = productData.get();
            product.setIdProductLine(products.getIdProductLine());
            product.setOrderDetails(products.getOrderDetails());
            product.setProductCode(products.getProductCode());
            product.setProductDescripttion(products.getProductDescripttion());
            product.setProductName(products.getProductName());
            product.setProductScale(products.getProductScale());
            product.setProductVendor(products.getProductVendor());
            product.setQuantityInStock(products.getQuantityInStock());
            Product saveProduct = productRepository.save(product);
            try {
                return new ResponseEntity<>(saveProduct, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<Product> deleteProductById(@PathVariable("id") int id) {
		try {
			productRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
