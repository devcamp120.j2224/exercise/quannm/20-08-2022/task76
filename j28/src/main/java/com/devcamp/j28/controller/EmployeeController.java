package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j28.entity.Employee;
import com.devcamp.j28.repository.IEmployeeRepository;

@RestController
@CrossOrigin
public class EmployeeController {
    @Autowired
    IEmployeeRepository employeeRepository;

    @GetMapping("/employees")
    public List<Employee> getAllEmployee() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if (employeeData.isPresent()) {
            return employeeData.get();
        } else {
            return null;
        }
    }

    @PostMapping("/employees")
    public ResponseEntity<Object> createNewEmployee(@RequestBody Employee employees) {
        try {
            Employee employee = new Employee();
            employee.setEmail(employees.getEmail());
            employee.setExtension(employees.getExtension());
            employee.setFirstName(employees.getFirstName());
            employee.setJobTitle(employees.getJobTitle());
            employee.setLastName(employees.getLastName());
            employee.setOfficeCode(employees.getOfficeCode());
            employee.setReportTo(employees.getReportTo());
            Employee newEmployee = employeeRepository.save(employee);
            return new ResponseEntity<>(newEmployee, HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id, @RequestBody Employee employees) {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if (employeeData.isPresent()) {
            Employee employee = employeeData.get();
            employee.setEmail(employees.getEmail());
            employee.setExtension(employees.getExtension());
            employee.setFirstName(employees.getFirstName());
            employee.setJobTitle(employees.getJobTitle());
            employee.setLastName(employees.getLastName());
            employee.setOfficeCode(employees.getOfficeCode());
            employee.setReportTo(employees.getReportTo());
            Employee saveEmployee = employeeRepository.save(employee);
            try {
                return new ResponseEntity<>(saveEmployee, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Employee> deleteEmployeeById(@PathVariable("id") int id) {
		try {
			employeeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
