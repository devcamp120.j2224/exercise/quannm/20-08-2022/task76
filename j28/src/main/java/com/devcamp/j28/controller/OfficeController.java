package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j28.entity.Office;
import com.devcamp.j28.repository.IOfficeRepository;

@RestController
@CrossOrigin
public class OfficeController {
    @Autowired
    IOfficeRepository officeRepository;

    @GetMapping("/offices")
    public List<Office> getAllProductLine() {
        return officeRepository.findAll();
    }

    @GetMapping("/offices/{id}")
    public Office getOfficeById(@PathVariable int id) {
        Optional<Office> officeData = officeRepository.findById(id);
        if (officeData.isPresent()) {
            return officeData.get();
        } else {
            return null;
        }
    }

    @PostMapping("/offices")
    public ResponseEntity<Object> createNewOffice(@RequestBody Office offices) {
        try {
            Office office = new Office();
            office.setAddressLine(offices.getAddressLine());
            office.setCity(offices.getCity());
            office.setCountry(offices.getCountry());
            office.setPhone(offices.getPhone());
            office.setState(offices.getState());
            office.setTerritory(offices.getTerritory());
            Office newOffice = officeRepository.save(office);
            return new ResponseEntity<>(newOffice, HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/offices/{id}")
    public ResponseEntity<Object> updateOffice(@PathVariable("id") int id, @RequestBody Office offices) {
        Optional<Office> officeData = officeRepository.findById(id);
        if (officeData.isPresent()) {
            Office office = officeData.get();
            office.setAddressLine(offices.getAddressLine());
            office.setCity(offices.getCity());
            office.setCountry(offices.getCountry());
            office.setPhone(offices.getPhone());
            office.setState(offices.getState());
            office.setTerritory(offices.getTerritory());
            Office saveOffice = officeRepository.save(office);
            try {
                return new ResponseEntity<>(saveOffice, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/offices/{id}")
    public ResponseEntity<Office> deleteOfficeById(@PathVariable("id") int id) {
		try {
			officeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
