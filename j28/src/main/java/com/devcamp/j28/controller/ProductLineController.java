package com.devcamp.j28.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j28.entity.ProductLine;
import com.devcamp.j28.repository.IProductLineRepository;

@RestController
@CrossOrigin
public class ProductLineController {
    @Autowired
    IProductLineRepository productLineRepository;

    @GetMapping("/productlines")
    public List<ProductLine> getAllProductLine() {
        return productLineRepository.findAll();
    }

    @GetMapping("/productlines/{id}")
    public ProductLine getProductLineById(@PathVariable int id) {
        Optional<ProductLine> productLineData = productLineRepository.findById(id);
        if (productLineData.isPresent()) {
            return productLineData.get();
        } else {
            return null;
        }
    }

    @PostMapping("/productlines")
    public ResponseEntity<Object> createNewProductLine(@RequestBody ProductLine productlines) {
        try {
            ProductLine productline = new ProductLine();
            productline.setDescription(productlines.getDescription());
            productline.setProductLine(productlines.getProductLine());
            productline.setProducts(productlines.getProducts());
            ProductLine newProductLine = productLineRepository.save(productline);
            return new ResponseEntity<>(newProductLine, HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/productlines/{id}")
    public ResponseEntity<Object> updateProductLine(@PathVariable("id") int id, @RequestBody ProductLine productlines) {
        Optional<ProductLine> productLineData = productLineRepository.findById(id);
        if (productLineData.isPresent()) {
            ProductLine productline = productLineData.get();
            productline.setDescription(productlines.getDescription());
            productline.setProductLine(productlines.getProductLine());
            productline.setProducts(productlines.getProducts());
            ProductLine saveProductLine = productLineRepository.save(productline);
            try {
                return new ResponseEntity<>(saveProductLine, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/productlines/{id}")
    public ResponseEntity<ProductLine> deleteProductLineById(@PathVariable("id") int id) {
		try {
			productLineRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
