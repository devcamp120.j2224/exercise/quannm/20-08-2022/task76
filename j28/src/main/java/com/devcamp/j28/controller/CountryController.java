package com.devcamp.j28.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j28.model.CCountry;
import com.devcamp.j28.repository.CountryRepository;

@RestController
@CrossOrigin
public class CountryController {
    @Autowired
    CountryRepository pCountryRepository;

    @GetMapping("/country1/{countryName}")
    public ResponseEntity<List<CCountry>> getAllCountryByCountryNameLike1(@PathVariable String countryName) {
        try {
            List<CCountry> countries = new ArrayList<CCountry>();
            pCountryRepository.findCountryByCountryNameLike1(countryName).forEach(countries::add);
            return new ResponseEntity<List<CCountry>>(countries, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country2/{countryName}")
    public ResponseEntity<List<CCountry>> getAllCountryByCountryNameLike2(@PathVariable String countryName) {
        try {
            List<CCountry> countries = new ArrayList<CCountry>();
            pCountryRepository.findCountryByCountryNameLike2(countryName).forEach(countries::add);
            return new ResponseEntity<List<CCountry>>(countries, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/country3/{countryName}")
    public ResponseEntity<List<CCountry>> getAllCountryByCountryNameLike3(@PathVariable String countryName) {
        try {
            List<CCountry> countries = new ArrayList<CCountry>();
            pCountryRepository.findCountryByCountryNameLike3(countryName).forEach(countries::add);
            return new ResponseEntity<List<CCountry>>(countries, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
