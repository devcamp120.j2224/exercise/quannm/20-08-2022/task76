package com.devcamp.j28.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.entity.Order;

@Repository
public interface IOrderRepository extends JpaRepository<Order, Integer> {
    @Query(value = "SELECT * FROM orders WHERE status LIKE :status%", nativeQuery = true)
	List<Order> findByStatusLike(@Param("status") String status);

	@Query(value = "SELECT * FROM orders WHERE customer_id LIKE :customer_id%", nativeQuery = true)
	List<Order> findByCustomerIdLike(@Param("customer_id") String customer_id);

	@Transactional
	@Modifying
	@Query(value = "UPDATE orders SET customer_id = :customer_id  WHERE customer_id IS null", nativeQuery = true)
	int updateCustomerId(@Param("customer_id") String customer_id);
}
