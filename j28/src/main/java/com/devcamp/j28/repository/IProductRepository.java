package com.devcamp.j28.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.entity.Product;

@Repository
public interface IProductRepository extends JpaRepository<Product, Integer> {
    @Query(value = "SELECT * FROM products WHERE product_code LIKE :productCode%", nativeQuery = true)
	List<Product> findByProductCodeLike(@Param("productCode") String productCode);

	@Query(value = "SELECT * FROM products WHERE quantity_in_stock LIKE :quantity_in_stock%", nativeQuery = true)
	List<Product> findByQuantityInStockLike(@Param("quantity_in_stock") String quantity_in_stock);

	@Transactional
	@Modifying
	@Query(value = "UPDATE products SET product_line_id = :product_line_id  WHERE product_line_id IS null", nativeQuery = true)
	int updateProductLineId(@Param("product_line_id") String product_line_id);
}
