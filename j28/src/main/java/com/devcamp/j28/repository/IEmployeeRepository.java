package com.devcamp.j28.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.entity.Employee;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface IEmployeeRepository extends JpaRepository<Employee, Integer> {
    @Query(value = "SELECT * FROM employees WHERE last_name LIKE :lastName%", nativeQuery = true)
	List<Employee> findByLastNameLike(@Param("lastName") String lastName);

	@Query(value = "SELECT * FROM employees WHERE first_name LIKE :firstName%", nativeQuery = true)
	List<Employee> findByFirstNameLike(@Param("firstName") String firstName);

	@Query(value = "SELECT * FROM employees WHERE office_code LIKE :office_code%", nativeQuery = true)
	List<Employee> findByOfficeCodeLike(@Param("office_code") String office_code);

	@Query(value = "SELECT * FROM employees WHERE job_title LIKE :job_title%", nativeQuery = true)
	List<Employee> findByJobTitleLike(@Param("job_title") String job_title);

	@Query(value = "SELECT * FROM employees WHERE report_to LIKE :report_to%", nativeQuery = true)
	List<Employee> findByReportToLike(@Param("report_to") String report_to);

	@Transactional
	@Modifying
	@Query(value = "UPDATE employees SET office_code = :office_code  WHERE office_code IS null", nativeQuery = true)
	int updateOfficeCode(@Param("office_code") String office_code);
}
