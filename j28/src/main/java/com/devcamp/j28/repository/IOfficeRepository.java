package com.devcamp.j28.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.entity.Office;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

@Repository
public interface IOfficeRepository extends JpaRepository<Office, Integer> {
    @Query(value = "SELECT * FROM offices WHERE city LIKE :city%", nativeQuery = true)
	List<Office> findByCityLike(@Param("city") String city);

	@Query(value = "SELECT * FROM offices WHERE state LIKE :state%", nativeQuery = true)
	List<Office> findByStateLike(@Param("state") String state);

	@Query(value = "SELECT * FROM offices WHERE country LIKE :country%", nativeQuery = true)
	List<Office> findByCountryLike(@Param("country") String country);

	@Query(value = "SELECT * FROM offices WHERE territory LIKE :territory%", nativeQuery = true)
	List<Office> findByTerritoryLike(@Param("territory") String territory);

	@Transactional
	@Modifying
	@Query(value = "UPDATE offices SET country = :country  WHERE country IS null", nativeQuery = true)
	int updateCountry(@Param("country") String country);
}
