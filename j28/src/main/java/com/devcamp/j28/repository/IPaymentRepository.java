package com.devcamp.j28.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.entity.Payment;

@Repository
public interface IPaymentRepository extends JpaRepository<Payment, Integer> {
    @Query(value = "SELECT * FROM payments WHERE check_number LIKE :checkNumber%", nativeQuery = true)
	List<Payment> findByCheckNumberLike(@Param("checkNumber") String checkNumber);

	@Query(value = "SELECT * FROM payments WHERE customer_id LIKE :customerId%", nativeQuery = true)
	List<Payment> findByCustomerIdLike(@Param("customerId") String customerId);

	@Transactional
	@Modifying
	@Query(value = "UPDATE payments SET customer_id = :customer_id  WHERE customer_id IS null", nativeQuery = true)
	int updateCustomerId(@Param("customer_id") String customer_id);
}
