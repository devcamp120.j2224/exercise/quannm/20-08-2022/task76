package com.devcamp.j28.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.model.CCountry;

@Repository
public interface CountryRepository extends JpaRepository<CCountry, Long> {
    @Query(value = "SELECT * FROM p_country WHERE country_name LIKE :countryName%", nativeQuery = true)
    List<CCountry> findCountryByCountryNameLike1(@Param("countryName") String countryName);

    @Query(value = "SELECT * FROM p_country WHERE country_name LIKE %:countryName", nativeQuery = true)
    List<CCountry> findCountryByCountryNameLike2(@Param("countryName") String countryName);

    @Query(value = "SELECT * FROM p_country WHERE country_name LIKE %:countryName%", nativeQuery = true)
    List<CCountry> findCountryByCountryNameLike3(@Param("countryName") String countryName);
}
