package com.devcamp.j28.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.model.CVoucher;

@Repository
public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    CVoucher findByMaVoucher(String maVoucher);

    @Transactional
    @Modifying
    @Query(value = "UPDATE p_vouchers SET phan_tram_giam_gia = :phanTram WHERE ma_voucher = :ma_voucher", nativeQuery = true)
    int updatePT(@Param("ma_voucher") String maVoucher, @Param("phanTram") String phanTram);
}
