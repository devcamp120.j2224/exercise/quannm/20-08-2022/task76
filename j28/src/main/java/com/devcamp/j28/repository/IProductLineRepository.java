package com.devcamp.j28.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.j28.entity.ProductLine;

@Repository
public interface IProductLineRepository extends JpaRepository<ProductLine, Integer> {
    @Query(value = "SELECT * FROM product_lines WHERE product_line LIKE :product_line%", nativeQuery = true)
	List<ProductLine> findByProductLineLike(@Param("product_line") String product_line);

	@Query(value = "SELECT * FROM product_lines WHERE description LIKE :description%", nativeQuery = true)
	List<ProductLine> findByDescriptionLike(@Param("description") String description);
}
